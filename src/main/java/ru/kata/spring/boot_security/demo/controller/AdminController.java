package ru.kata.spring.boot_security.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.service.RoleService;
import ru.kata.spring.boot_security.demo.service.UserDetailsServiceImpl;
import ru.kata.spring.boot_security.demo.service.UserService;

import java.util.ArrayList;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public AdminController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    // Метод для главной страницы, сразу при помощи View отображаю весь список из БД
    @GetMapping("/index")
    public String indexPage(Model model) {
        model.addAttribute("empAll", userService.getAllEmployee());
        model.addAttribute("userLog", UserDetailsServiceImpl.getUserLog().getName());
        return "pageAdmin";
    }

    // при обращении к /formUserAdd/new срабатывает данный метод, создаю пустую сущность и при помощи
    // Класса Model помещаю туда сущность с дефолтными значениями. id Model user.
    // HTML форма для добавления новой записи
    @GetMapping("/formUserAdd/new")
    public String formUserAdd(Model model) {
        model.addAttribute("user", new MyUser());
        model.addAttribute("roleList", roleService.findRoleAll().stream().toList());
        model.addAttribute("userLog", UserDetailsServiceImpl.getUserLog().getName());
        return "formUserAdd";
    }

    //Добавляю в форму Вьюшки с дефолтными значениями (новые значения), создаю новый объект!
    //Возвращяюсь на главную страницу
    @PostMapping("/addUser")
    public String addUser(@ModelAttribute("user") MyUser myUser,
                          @RequestParam(value = "rol", required = false) ArrayList<Long> role) {
        MyUser useradd = userService.userForSave(myUser, roleService.buildRoleSet(role));
        if (useradd != null) {
            userService.add(useradd);
            return "redirect:/admin/index";
        } else return "formUserAddError";
    }

    // HTML форма для редактирования уже существующей записи
    @GetMapping("/formUserEdit/edit")
    public String formUserEdit(@RequestParam("updateIdUser") Long id, Model model) {
        MyUser userFind = userService.findUser(id);
        model.addAttribute("user", userFind);
        model.addAttribute("role", roleService.findRole(userFind));
        model.addAttribute("roleList", roleService.findRoleAll().stream().toList());
        return "formUserEdit";
    }

    // Изменяю
    @PatchMapping("/update")
    public String updateEmployee(@ModelAttribute("user") MyUser myUser,
                                 @RequestParam(value = "rol", required = false) ArrayList<Long> role) {
        MyUser userUpdate = userService.userForUpdate(myUser, roleService.buildRoleSet(role));
        if (userUpdate != null) {
            userService.updateUser(userUpdate);
            return "redirect:/admin/index";
        } else return "formUserEditError";
    }

    // Удаляю Юзера
    @DeleteMapping("/delete/user")
    public String deleteEmployee(@RequestParam("deleteIdUser") Long id) {
        userService.deleteUser(id);
        return "redirect:/admin/index";
    }

}
