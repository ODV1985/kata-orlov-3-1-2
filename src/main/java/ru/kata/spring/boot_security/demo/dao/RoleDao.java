package ru.kata.spring.boot_security.demo.dao;

import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;

import java.util.ArrayList;
import java.util.Set;

public interface RoleDao {

    Set<Role> findRole(MyUser myUser);

    Set<Role> findRoleAll();

    Role findRole(Long id);

    void deleteRole(Long id);

    Set<Role> buildRoleSet(ArrayList<Long> role);
}
