package ru.kata.spring.boot_security.demo.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    public void addViewControllers(ViewControllerRegistry registry) {
        //Тут вешаю на страницу свою форму так как стандартная не устраивает,
        //буду писать свою, но стандартная логика секьюрити устраивает.
        registry.addViewController("/login").setViewName("login");
    }
}
