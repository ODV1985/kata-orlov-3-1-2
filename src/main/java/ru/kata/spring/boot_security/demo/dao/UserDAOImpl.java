package ru.kata.spring.boot_security.demo.dao;

import org.springframework.stereotype.Repository;
import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Repository
public class UserDAOImpl implements UserDao {

    //PersistenceContext
    //1. Гарантия того, что сущности будут сохранены в БД.
    //2. Отслеживает изменения и отправляет их в БД.
    //3. Работает как кеш
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<MyUser> getAllEmployee() {
        TypedQuery<MyUser> query = entityManager.createQuery("select u from MyUser u", MyUser.class);
        try {
            return query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public MyUser findUser(Long id) {
        return entityManager.find(MyUser.class, id);
    }

    @Override
    public MyUser findUser(String username) {
        TypedQuery<MyUser> query = entityManager
                .createQuery("select u from MyUser u where u.useName = :un", MyUser.class)
                .setParameter("un", username);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void add(MyUser myUser) {
        entityManager.persist(myUser);
    }

    @Override
    public void updateUser(MyUser myUser) {
            entityManager.merge(myUser);
    }

    @Override
    public void deleteUser(Long id) {
        MyUser myUser = findUser(id);
        entityManager.remove(myUser);
    }

}
