package ru.kata.spring.boot_security.demo.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "users")
public class MyUser implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "age")
    private int age;

    @Column(name = "username")
    private String useName;

    @Column(name = "password")
    private String pass;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "authorities",
            joinColumns = @JoinColumn(name = "iduser"),
            inverseJoinColumns = @JoinColumn(name = "idrole"))
    private Set<Role> roleSet;

    public MyUser() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUseName() {
        return useName;
    }

    public void setUseName(String useName) {
        this.useName = useName;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Set<Role> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<Role> roleSet) {
        this.roleSet = roleSet;
    }

    //Указывает, истек ли срок действия учетной записи пользователя.
    //true если учетная запись пользователя действительна
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    //Указывает, заблокирован или разблокирован пользователь.
    //true если пользователь не заблокирован
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //Указывает, истек ли срок действия учетных данных (пароля) пользователя
    //true если учетные данные пользователя действительны
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //Указывает, включен или отключен пользователь.
    //true если пользователь включен
    @Override
    public boolean isEnabled() {
        return true;
    }

    //Возвращает роли, предоставленные пользователю.
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoleSet();
    }

    //Пароль
    @Override
    public String getPassword() {
        return getPass();
    }

    //Логин
    @Override
    public String getUsername() {
        return getUseName();
    }

}
