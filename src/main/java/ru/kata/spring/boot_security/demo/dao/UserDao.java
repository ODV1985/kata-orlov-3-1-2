package ru.kata.spring.boot_security.demo.dao;

import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;

import java.util.List;
import java.util.Set;

public interface UserDao {
    void add(MyUser myUser);

    void updateUser(MyUser myUser);

    List<MyUser> getAllEmployee();

    void deleteUser(Long id);

    MyUser findUser(Long id);

    MyUser findUser(String username);

}
