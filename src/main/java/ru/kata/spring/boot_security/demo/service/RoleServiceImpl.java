package ru.kata.spring.boot_security.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kata.spring.boot_security.demo.dao.RoleDao;
import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;

import java.util.ArrayList;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleDao roleDao;

    @Autowired
    public RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public Set<Role> findRole(MyUser myUser) {
        return roleDao.findRole(myUser);
    }

    @Override
    public Set<Role> findRoleAll() {
        return roleDao.findRoleAll();
    }

    @Override
    public Role findRole(Long id) {
        return roleDao.findRole(id);
    }

    @Override
    @Transactional
    public void deleteRole(Long id) {
        roleDao.deleteRole(id);
    }

    @Override
    public Set<Role> buildRoleSet(ArrayList<Long> role) {
        return roleDao.buildRoleSet(role);
    }
}
