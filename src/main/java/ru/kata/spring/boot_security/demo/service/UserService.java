package ru.kata.spring.boot_security.demo.service;

import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;

import java.util.List;
import java.util.Set;

public interface UserService {
    void add(MyUser myUser);

    MyUser userForSave(MyUser myUser, Set<Role> roleSet);

    MyUser userForUpdate(MyUser myUser, Set<Role> roleSet);

    void updateUser(MyUser myUser);

    void deleteUser(Long id);

    List<MyUser> getAllEmployee();

    MyUser findUser(Long id);

    MyUser findUser(String username);
}
