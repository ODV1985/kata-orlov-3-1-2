package ru.kata.spring.boot_security.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.kata.spring.boot_security.demo.entity.MyUser;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserServiceImpl userService;
    private static MyUser userLog;

    @Autowired
    public UserDetailsServiceImpl(UserServiceImpl userService) {
        this.userService = userService;
    }


    //Реализовываю данный метод согласно условию задачи и использую его в классе WebSecurityConfig
    //для Аутентификации юзера! данный метод возвращяет узера с 3 данными!!!
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        userLog = userService.findUser(username);
        return userService.findUser(username);
    }

    public static MyUser getUserLog() {
        return userLog;
    }
}
