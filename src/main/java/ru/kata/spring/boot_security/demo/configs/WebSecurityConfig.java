package ru.kata.spring.boot_security.demo.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final SuccessUserHandler successUserHandler;
    private final UserDetailsService userDetailsService;

    @Autowired
    public WebSecurityConfig(SuccessUserHandler successUserHandler, UserDetailsService userDetailsService) {
        this.successUserHandler = successUserHandler;
        this.userDetailsService = userDetailsService;
    }

    //переопределяю метод и использую в нем данные юзера по 3 данным.
    //про данный метод рассказывает Алишев в 94 уроке Хранение пароля в БД в зашифрованном формате!!!
    //Тут делаю Аутоинтефикацию юзера
    //Плюс кодировка пароля
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    //Тут делаю Авторизацию юзера согласно его ролей и перенаправляю его с помощью SuccessUserHandler
    //на доступную ему страничку
    //прописываю кому куда есть доступ
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //кодировка
                .csrf().disable()
                //вкл авторизацию
                .authorizeRequests()
                //тут по данному URL разрешаю доступ всем
                .antMatchers("/login").permitAll()
                //тут ограничиваю URL по ролям
                .antMatchers("/admin/**").hasRole("ADMINISTRATOR")
                .antMatchers("/user/**").hasAnyRole("ADMINISTRATOR", "USER")
                .anyRequest().authenticated()
                .and()
                //вкл форму логин
                .formLogin().loginPage("/login").permitAll()
                //тут указываю логинам их роли в Хендлере и на какую стр они будут перенаправленны
                .successHandler(successUserHandler)
                //тут разрешаем этим пользоватся всем
                .permitAll()
                .and()
                //тут вкл логаут
                .logout()
                //разрешаем им пользоватся всем
                .permitAll();
    }

    // Бин кодера
    @Bean
    public static BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}
