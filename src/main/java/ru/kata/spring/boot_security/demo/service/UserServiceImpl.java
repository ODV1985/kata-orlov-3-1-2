package ru.kata.spring.boot_security.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kata.spring.boot_security.demo.dao.UserDao;
import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional
    public void add(MyUser myUser) {
        userDao.add(myUser);
    }

    @Override
    public List<MyUser> getAllEmployee() {
        return userDao.getAllEmployee();
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        userDao.deleteUser(id);
    }

    @Override
    public MyUser findUser(Long id) {
        return userDao.findUser(id);
    }

    @Override
    public MyUser findUser(String username) {
        return userDao.findUser(username);
    }

    @Override
    @Transactional
    public void updateUser(MyUser myUser) {
        userDao.updateUser(myUser);
    }

    @Override
    public MyUser userForSave(MyUser myUser, Set<Role> roleSet) {
        String userName = myUser.getUseName();
        MyUser user = findUser(userName);
        if (user == null) {
            user = myUser;
            user.setRoleSet(roleSet);
            myUser.setPass(bCryptPasswordEncoder.encode(myUser.getPass()));
            return user;
        } else return null;
    }

    @Override
    public MyUser userForUpdate(MyUser myUser, Set<Role> roleSet) {
        String userName = myUser.getUseName();
        MyUser user = findUser(userName);

        if ((user == null) || (user.getId() == myUser.getId() && user.getUsername().equals(myUser.getUsername()))) {
            user = myUser;
            user.setRoleSet(roleSet);
            myUser.setPass(user.getPass());
            return user;
        } else return null;
    }

}
